'use strict';

var gulp = require('gulp'),
	gulpLoadPlugins = require('gulp-load-plugins'),
	$ = gulpLoadPlugins(),
	pug = require('gulp-pug'),
	data = require('gulp-data'),
	notify = require('gulp-notify'),
	fs = require('fs'),
	path = require('path'),
	merge = require('gulp-merge-json'),
	plumber = require('gulp-plumber'),
	debug = require('gulp-debug'),
	multipipe = require('multipipe');


module.exports = function(options) {

	return function() {
		return multipipe (
			gulp.src(options.src),
			$.plumber({
				errorHandler: notify.onError(function(err){
					return{
						title: 'data error',
						massage:err.massage
					};
				})
			}),
			merge({
				fileName: 'data.json',
				edit: (json, file) => {
					// Extract the filename and strip the extension
					var filename = path.basename(file.path),
					primaryKey = filename.replace(path.extname(filename), '');

					// Set the filename as the primary key for our JSON data
					var data = {};
					data[primaryKey.toUpperCase()] = json;

					return data;
				}
			}),
			$.debug({title:'JSON FILES MERGE'}),
			gulp.dest(options.dest)
		);
	};

};
