/*----------------------------------------
 INIT AOS ANIMATE
 ----------------------------------------*/

AOS.init({
  duration: 1000,
  disable: 'mobile',
  once: false,
  anchorPlacement: 'top-bottom'
});

/*----------------------------------------
 TRANSITION SCROLL
 ----------------------------------------*/
$('.scroll').on('click', function (e) {
  // e.preventDefault();
  var anchor = $(this)
  $('html, body').stop().animate({
    scrollTop: $(anchor.attr('href')).offset().top - 100
  }, 1000)
})
/*----------------------------------------
  PHONE MASK
----------------------------------------*/
$(".js-mask-number").mask("0#");
$(".js-mask-phone").mask("+7 (000) 000-00-00");
$(".js-mask-time").mask("00 : 00");
$(".js-mask-date").mask("00.00.0000");

/*----------------------------------------
  SELECTIZE INIT
----------------------------------------*/

$('.sumo-select').SumoSelect();

/*----------------------------------------
  MENU
----------------------------------------*/

var buttonOpenMenu = $('.js-menu-open'),
    buttonOpenClose = $('.js-menu-close'),
    menu = $('.js-menu'),
    menuLink = $('.js-menu a.scroll');

$('<div>', { class: 'menu-ovelay'}).appendTo('.header');

function openMenu (e) {
  e.preventDefault();

  menu.addClass('menu_open');
  $('.menu-ovelay').addClass('menu-ovelay_visible');
  $('body').addClass('menu-open');
}

function closeMenu (e) {
  e.preventDefault();

  menu.removeClass('menu_open');
  $('.menu-ovelay').removeClass('menu-ovelay_visible');
  $('body').removeClass('menu-open');
}

buttonOpenMenu.click(openMenu);
menuLink.click(closeMenu);
buttonOpenClose.click(closeMenu);
$('body .menu-ovelay').click(closeMenu);


/*----------------------------------------
  SLICK INIT
----------------------------------------*/

var reviewsCarousel = $('.js-reviews');

reviewsCarousel.slick({
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: true,
  dots:true,
  fade: true,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        fade:false
      }
    },
  ]
});
