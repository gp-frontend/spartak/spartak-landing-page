$(function () {

  var link = $('.js-menu a');

  // Run the scrNav when scroll
  $(window).on('scroll', function () {
    scrNav()
  });

  // scrNav function
  // Change active dot according to the active section in the window
  function scrNav () {
    var sTop = $(window).scrollTop()

    $('section').each(function () {

      var id = $(this).attr('id'),
        offset = $(this).offset().top - 1,
        height = $(this).height()

      if (sTop >= offset && sTop < offset + height) {
        link.removeClass('active')
        $('.js-menu').find('a[href="#' + id + '"]').addClass('active');
      }
    })
  }

  scrNav()
});
